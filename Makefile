.PHONY: test analysis build publish clean

TARGETS = windows linux

test: cmd/main.go
	docker-compose -f build/docker-compose/docker-compose.yml up -d --force-recreate
	MOBWA_API_HOST="0.0.0.0" \
	MOBWA_API_PORT="1111" \
	MOBWA_DB_HOST="127.0.0.1" \
	MOBWA_DB_PORT="15432" \
	MOBWA_DB_NAME="mobwa" \
	MOBWA_DB_PASSWORD="mobwa" \
	MOBWA_DB_USERNAME="mobwa" \
	go test ./... -v -coverprofile coverage.out
	docker-compose -f build/docker-compose/docker-compose.yml down

ci-test: cmd/main.go
	go test ./... -v -coverprofile coverage.out

analysis:
	sonar-scanner \
	  -Dsonar.projectKey=mobwa_mobwa-api-server \
	  -Dsonar.organization=mobwa \
	  -Dsonar.go.coverage.reportPaths=coverage.out \
	  -Dsonar.branch.name=${CI_COMMIT_BRANCH} \
	  -Dsonar.exclusions="${SONAR_EXCLUSIONS}"

build:
	cd internal/server/db/migrations/ && go-bindata -pkg migrations .
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/mobwa-api-server cmd/main.go

publish:
	echo "publish"

clean:
	rm -rf bin

assets:
	cd internal/server/db/migrations/ && go-bindata -pkg migrations .

run: build
	docker-compose up --build

down: build
	docker-compose down

format:
	go fmt -s -w ./

docker-build:
	docker build -t mobwa/mobwa-api-server:dev .

docker-publish: docker-build
	docker push -t mobwa/mobwa-api-server:dev