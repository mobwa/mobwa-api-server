module gitlab.com/mobwa/mobwa-api-server

go 1.16

replace gitlab.com/mobwa/mobwa-api-server => ./

require (
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgconn v1.10.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/lib/pq v1.10.2 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/prometheus/tsdb v0.7.1 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
)
