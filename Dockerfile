FROM gcr.io/distroless/base-debian10

COPY bin/mobwa-api-server /usr/bin/mobwa-api-server

EXPOSE 1111/tcp

ENTRYPOINT ["mobwa-api-server"]