# Mobwa Payments Hub


[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=mobwa_mobwa-api-server)

----------------------------------------

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. 

Mobwa Payments Hub currently supports the following currencies:

* United States Dollar (USD)
* Singapore Dollar (SGD)

Mobwa Payments Hub runs on Linux.

## Related Links

* [API Reference](https://mobwa.gitlab.io/mobwa-api-spec)
* SDKs
  * [Ruby SDK](https://gitlab.com/mobwa/mobwa-sdk-for-ruby)
  * [Python SDK](https://gitlab.com/mobwa/mobwa-sdk-for-python)
  * [Node SDK](https://gitlab.com/mobwa/mobwa-sdk-for-javascript)
  * [Go SDK](https://gitlab.com/mobwa/mobwa-sdk-for-go)
  * [Perl SDK](https://gitlab.com/mobwa/mobwa-sdk-for-perl)
  * [Rust SDK](https://gitlab.com/mobwa/mobwa-sdk-for-rust)
* [Code Quality Metrics](https://sonarcloud.io/dashboard?id=mobwa_mobwa-api-server)
* [Docker Images](https://hub.docker.com/r/mobwa/mobwa-api-server)
* [Helm chart](https://mobwa.gitlab.io/charts/index.yaml)

## Quick Start
### Using docker
```sh
docker pull mobwa/mobwa-api-server
docker run mobwa-api-server --detach
  -e MOBWA_DB_HOST=${DB_HOST} \
  -e MOBWA_DB_PORT=${DB_PORT} \
  -e MOBWA_DB_USER=${DB_USER} \
  -e MOBWA_DB_PASSWORD=${DB_PASSWORD} \
  -e MOBWA_DB_NAME=${DB_NAME}
```

### Using docker compose
```sh
docker-compose up --build
```
### Using Helm chart
```sh
helm repo add mobwa https://mobwa.gitlab.io/charts
helm install mobwa mobwa/mobwa-api-server \
  --env MOBWA_DB_HOST=${DB_HOST} \
  --env MOBWA_DB_PORT=${DB_PORT} \
  --env MOBWA_DB_USER=${DB_USER} \
  --env MOBWA_DB_PASSWORD=${DB_PASSWORD} \
  --env MOBWA_DB_NAME=${DB_NAME}
```

## Contributing

Thank you for your interest in contributing! Please refer to [CONTRIBUTING.md](https://gitlab.com/mobwa/mobwa-api-server/-/blob/develop/CONTRIBUTING.md) for guidance.

### Running the unit tests

To run unit tests you need to have installed Golang toolkit, Docker and Docker Compose.

After ensuring thatyou can just issue the following command:

```
$ make test
```