package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/mobwa/mobwa-api-server/internal/cmd"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
)

func executeServer(t *testing.T) {
	server := cmd.New(nil)
	server.SetArgs(
		[]string{
			"--api-host", os.Getenv("MOBWA_API_HOST"),
			"--api-port", os.Getenv("MOBWA_API_PORT"),
			"--db-host", os.Getenv("MOBWA_DB_HOST"),
			"--db-port", os.Getenv("MOBWA_DB_PORT"),
			"--db-user", os.Getenv("MOBWA_DB_USERNAME"),
			"--db-password", os.Getenv("MOBWA_DB_PASSWORD"),
			"--db-name", os.Getenv("MOBWA_DB_NAME"),
		},
	)

	go func() {
		if err := server.Execute(); err != nil {
			t.Fatal(err)
		}
	}()
}

func createUser(data *models.CreateUserRequest) (*http.Response, *models.User, error) {
	req, err := json.Marshal(data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := http.Post("http://127.0.0.1:1111/signup", "application/json", bytes.NewBuffer(req))
	if err != nil {
		return nil, nil, err
	}

	var user models.User

	if err := json.NewDecoder(resp.Body).Decode(&user); err != nil {
		return resp, nil, err
	}

	return resp, &user, nil
}

func TestSignUp(t *testing.T) {
	executeServer(t)

	time.Sleep(10 * time.Second)

	signupRequest := models.SignupRequest{
		Email:    "customer-1@mobwa.com",
		Username: "customer1",
		Password: "123456",
	}

	data, err := json.Marshal(signupRequest)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.Post("http://127.0.0.1:1111/signup", "application/json", bytes.NewBuffer(data))
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusCreated {
		t.Errorf("Expected status code of %d but got %d", http.StatusCreated, resp.StatusCode)
	}

	var user models.User

	if err := json.NewDecoder(resp.Body).Decode(&user); err != nil {
		t.Error("Expected a right json document")
	}

	if len(user.Accounts) < 1 {
		t.Errorf("Should have created a default account for user with username %s", user.Username)
	} else {
		if user.Accounts[0].Balance != 0 {
			t.Errorf("The balance of the default account for user %s should be 0", user.ID)
		}

	}
}

func TestRechargeAccount(t *testing.T) {
	executeServer(t)

}
