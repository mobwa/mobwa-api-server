package app

import (
	"fmt"
	"os"

	cobra "github.com/spf13/cobra"
	cmd "gitlab.com/mobwa/mobwa-api-server/internal/cmd"
	"gitlab.com/mobwa/mobwa-api-server/pkg/cli"
)

var options = &cli.Options{}

const VERSION = "0.1.0-alpha.4"

type app struct {
	command *cobra.Command
}

func New() *app {
	return &app{
		command: cmd.New(options),
	}
}

func WithOpts(opts *cli.Options) *app {
	return &app{
		command: cmd.New(opts),
	}
}

func (a *app) Run() {
	if err := a.command.Execute(); err != nil {
		fmt.Fprint(os.Stderr, err)

		if e, ok := err.(*cli.Error); ok {
			os.Exit(e.Code)
		}

		os.Exit(1)
	}
}
