package cmd

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mobwa/mobwa-api-server/internal/config"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/db"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/router"
	"gitlab.com/mobwa/mobwa-api-server/pkg/cli"
)

var (
	ErrAPIHostNotDefined    = errors.New("API host must be defined")
	ErrAPIPortNotDefined    = errors.New("API port must be defined")
	ErrDBHostNotDefined     = errors.New("DB host must be defined")
	ErrDBPortNotDefined     = errors.New("DB port must be defined")
	ErrDBUserNotDefined     = errors.New("DB user must be defined")
	ErrDBPasswordNotDefined = errors.New("DB password must be defined")
	ErrDBNameNotDefined     = errors.New("DB name must be defined")
)

var (
	OptionAPIHost    = "api-host"
	OptionAPIPort    = "api-port"
	OptionDBHost     = "db-host"
	OptionDBPort     = "db-port"
	OptionDBUser     = "db-user"
	OptionDBPassword = "db-password"
	OptionDBName     = "db-name"

	EnvVarPrefix = "MOBWA"

	DefaultConfigFileName = "config"
	DefaultExtension      = "yaml"
)

func New(options *cli.Options) *cobra.Command {
	newCmd := &cobra.Command{
		Use:   "mobwa",
		Short: "mobwa",
		Long:  `mobwa`,
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := validateOpts(); err != nil {
				return err
			}

			srv := http.Server{
				Addr:    fmt.Sprintf("%s:%s", viper.Get(OptionAPIHost), viper.Get(OptionAPIPort)),
				Handler: router.Router(),
			}

			db.Init(
				viper.GetString(OptionDBUser),
				viper.GetString(OptionDBPassword),
				viper.GetString(OptionDBHost),
				viper.GetString(OptionDBPort),
				viper.GetString(OptionDBName),
			)

			log.Println(db.DSN())

			if err := db.Migrate(db.DSN()); err != nil {
				log.Fatalln(err)
			}

			if err := srv.ListenAndServe(); err != nil {
				log.Fatal(err)
			}

			return nil
		},
	}

	LoadConfig(".")

	newCmd.PersistentFlags().StringVar(&config.Config.API.Host, OptionAPIHost, "0.0.0.0", "Address to listen to")
	newCmd.PersistentFlags().StringVar(&config.Config.API.Port, OptionAPIPort, "1111", "Port to listen to")
	newCmd.PersistentFlags().StringVar(&config.Config.DB.Host, OptionDBHost, config.Config.DB.Host, "Database host address")
	newCmd.PersistentFlags().StringVar(&config.Config.DB.Port, OptionDBPort, "5432", "Database port")
	newCmd.PersistentFlags().StringVar(&config.Config.DB.User, OptionDBUser, config.Config.DB.User, "Database user")
	newCmd.PersistentFlags().StringVar(&config.Config.DB.Password, OptionDBPassword, config.Config.DB.Password, "Database password")
	newCmd.PersistentFlags().StringVar(&config.Config.DB.Name, OptionDBName, config.Config.DB.Name, "Database name")

	viper.BindEnv(OptionAPIHost, "MOBWA_API_HOST")
	viper.BindEnv(OptionAPIPort, "MOBWA_API_PORT")
	viper.BindEnv(OptionDBHost, "MOBWA_DB_HOST")
	viper.BindEnv(OptionDBPort, "MOBWA_DB_PORT")
	viper.BindEnv(OptionDBUser, "MOBWA_DB_USER")
	viper.BindEnv(OptionDBPassword, "MOBWA_DB_PASSWORD")
	viper.BindEnv(OptionDBName, "MOBWA_DB_NAME")

	viper.BindPFlag(OptionAPIHost, newCmd.PersistentFlags().Lookup("api-host"))
	viper.BindPFlag(OptionAPIPort, newCmd.PersistentFlags().Lookup("api-port"))
	viper.BindPFlag(OptionDBHost, newCmd.PersistentFlags().Lookup("db-host"))
	viper.BindPFlag(OptionDBPort, newCmd.PersistentFlags().Lookup("db-port"))
	viper.BindPFlag(OptionDBUser, newCmd.PersistentFlags().Lookup("db-user"))
	viper.BindPFlag(OptionDBPassword, newCmd.PersistentFlags().Lookup("db-password"))
	viper.BindPFlag(OptionDBName, newCmd.PersistentFlags().Lookup("db-name"))

	return newCmd
}

func LoadConfig(path string) error {
	viper.AddConfigPath(path)
	viper.SetConfigName(DefaultConfigFileName)
	viper.SetConfigType(DefaultExtension)

	viper.SetEnvPrefix("mobwa")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(&config.Config)
	if err != nil {
		return err
	}

	return nil
}

func validateOpts() error {
	if viper.Get(OptionAPIHost) == "" {
		return ErrAPIHostNotDefined
	}

	if viper.Get(OptionAPIPort) == "" {
		return ErrAPIPortNotDefined
	}

	if viper.Get(OptionDBHost) == "" {
		return ErrDBHostNotDefined
	}

	if viper.Get(OptionDBPort) == "" {
		return ErrDBPortNotDefined

	}

	if viper.Get(OptionDBUser) == "" {
		return ErrDBUserNotDefined
	}

	if viper.Get(OptionDBPassword) == "" {
		return ErrDBPasswordNotDefined
	}

	if viper.Get(OptionDBName) == "" {
		return ErrDBNameNotDefined
	}

	return nil
}
