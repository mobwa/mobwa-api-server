package features

import "errors"

var (
	ErrUserExists = errors.New("User already exists")
)

const (
	QueryGetAccountID             = `SELECT id FROM accounts WHERE uuid = $1 LIMIT 1`
	QueryGetAccountByID           = `SELECT uuid, name, balance, created_at, updated_at, currency FROM accounts WHERE id = $1 LIMIT 1`
	QueryGetAccount               = `SELECT uuid, name, balance, created_at, updated_at, currency FROM accounts WHERE uuid = $1 LIMIT 1`
	QueryGetUserAccounts          = `SELECT uuid, name, balance, created_at, updated_at, currency FROM accounts WHERE user_id = $1`
	QueryGetUserAccount           = `SELECT uuid, name, balance, created_at, updated_at, currency FROM accounts WHERE user_id = $1 AND uuid = $2 LIMIT 1`
	QueryCreateAccount            = `INSERT INTO accounts (uuid, name, user_id, created_at) VALUES ($1, $2, $3, $4)`
	QueryGetUserID                = `SELECT id FROM users WHERE uuid = $1 LIMIT 1`
	QueryGetUser                  = `SELECT uuid, username, email, is_admin, created_at, updated_at FROM users WHERE uuid = $1 LIMIT 1`
	QueryGetUserByEmail           = `SELECT uuid, username, email, is_admin, created_at, updated_at FROM users WHERE email = $1 LIMIT 1`
	QueryGetUserByUsername        = `SELECT uuid, username, email, is_admin, created_at, updated_at FROM users WHERE username = $1 LIMIT 1`
	QueryGetUserByUsernameOrEmail = `SELECT uuid, username, email, pwd, is_admin, created_at, updated_at FROM users WHERE (username = $1 OR email = $1) LIMIT 1`
	QueryUserExists               = `SELECT a.total > 0 FROM (SELECT COUNT (*) AS total FROM users WHERE (username = $1 OR email = $2) LIMIT 1) a`
	QueryCreateUser               = `INSERT INTO users (uuid, username, email, pwd, created_at) VALUES ($1, $2, $3, $4, $5)`
	QueryTransferAccount          = `INSERT INTO transfers (uuid, message, amount, currency, source, destination, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7)`
	QueryGetTransfer              = `SELECT  uuid, message, amount, status, source, destination, created_at, updated_at FROM transfers WHERE uuid = $1 AND source = $2`

	QueryWithdrawAccount  = `UPDATE accounts SET balance = balance - $1, updated_at = $3 WHERE uuid = $2 AND balance - $1 >= 0`
	QueryCompleteTransfer = "UPDATE transfers SET status = 'COMPLETE' WHERE uuid = $1"
)

const (
	RoleAdmin    = "ADMIN"
	RoleCustomer = "CUSTOMER"
)
