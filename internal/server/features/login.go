package features

import (
	"errors"
	"log"

	"gitlab.com/mobwa/mobwa-api-server/internal/server/db"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
	"golang.org/x/crypto/bcrypt"
)

func FindUser(name, password string) (*models.User, error) {
	conn := db.Default()

	row := conn.QueryRow(QueryGetUserByUsernameOrEmail, name)

	var user models.User

	var isAdmin bool
	var hash string

	err := row.Scan(&user.ID, &user.Username, &user.Email, &hash, &isAdmin, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if !checkPassword(hash, password) {
		log.Println(hash)

		return nil, errors.New("Invalid password")
	}

	if isAdmin {
		user.Role = RoleAdmin
	} else {
		user.Role = RoleCustomer
	}

	return &user, nil
}

func checkPassword(hash, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)) == nil
}
