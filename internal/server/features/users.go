package features

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/db"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(user models.CreateUserRequest) (*models.User, error) {
	conn := db.Default()

	id, _ := uuid.NewUUID()
	newId := id.String()

	password, err := bcrypt.GenerateFromPassword([]byte(user.Password), 14)
	if err != nil {
		return nil, err
	}

	exists, err := UserExists(user.Username, user.Email)
	if err != nil {
		return nil, err
	}

	if exists {
		return nil, ErrUserExists
	}

	_, err = conn.Exec(QueryCreateUser, newId, user.Username, user.Email, string(password), time.Now())
	if err != nil {
		return nil, err
	}

	newUser, err := GetUser(id.String())
	if err != nil {
		return nil, err
	}

	return newUser, nil
}

func getUser(query string, subject ...interface{}) (*models.User, error) {
	conn := db.Default()

	row := conn.QueryRow(query, subject...)

	var user models.User

	var isAdmin bool

	err := row.Scan(&user.ID, &user.Username, &user.Email, &isAdmin, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		return nil, err
	}

	if isAdmin {
		user.Role = RoleAdmin
	} else {
		user.Role = RoleCustomer
	}

	return &user, nil

}

func GetUser(id string) (*models.User, error) {
	return getUser(QueryGetUser, id)
}

func GetUserByEmail(id string) (*models.User, error) {
	return getUser(QueryGetUserByEmail, id)
}

func GetUserByUsername(id string) (*models.User, error) {
	return getUser(QueryGetUserByEmail, id)
}

func GetUserByUsernameOrEmail(username, email string) (*models.User, error) {
	return getUser(QueryGetUserByUsernameOrEmail, username, email)
}

func UserExists(username, email string) (bool, error) {
	conn := db.Default()

	row := conn.QueryRow(QueryUserExists, username, email)

	var exists bool

	err := row.Scan(&exists)
	if err != nil {
		return true, err
	}

	return exists, nil

}

func GetUserID(uuid string) (*int, error) {
	conn := db.Default()

	row := conn.QueryRow(QueryGetUserID, uuid)

	var uid int

	err := row.Scan(&uid)
	if err != nil {
		return nil, err
	}

	return &uid, nil

}
