package features

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/db"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
)

var (
	ErrDestinationAccountAbsent = errors.New("The destination account does not exist")
	ErrCurrencyMismatch         = errors.New("Source and destination account must be both in same currency")
	ErrInsuficientBalance       = errors.New("Insuficient balance")
)

func CreateTransfer(user *models.User, account *models.Account, req *models.CreateTransferRequest) (*models.Transfer, error) {
	conn := db.Default()

	id, _ := uuid.NewUUID()
	newId := id.String()

	sourceId, err := GetAccountID(account.ID)
	if err != nil {
		return nil, err
	}

	destinationId, err := GetAccountID(req.Destination)
	if err != nil {
		return nil, ErrDestinationAccountAbsent
	}

	destination, err := GetAccount(req.Destination)
	if err != nil {
		return nil, ErrDestinationAccountAbsent
	}

	if account.Currency != destination.Currency {
		return nil, ErrCurrencyMismatch
	}

	_, err = conn.Exec(QueryTransferAccount, newId, req.Message, req.Amount, req.Currency, *sourceId, *destinationId, time.Now())
	if err != nil {
		return nil, err
	}

	newTransfer, err := GetTransfer(id.String(), *sourceId)
	if err != nil {
		return nil, err
	}

	tx, err := conn.Tx()
	if err != nil {
		return nil, err
	}

	if account.Balance < req.Amount {
		return nil, ErrInsuficientBalance
	}

	if _, err = tx.Exec(context.Background(), QueryWithdrawAccount, newTransfer.Amount, account.ID, time.Now()); err != nil {
		tx.Rollback(context.Background())
		return nil, errors.New("TX -1")
	}

	if _, err = tx.Exec(context.Background(), QueryRechargeAccount, newTransfer.Amount, destination.ID, time.Now()); err != nil {
		tx.Rollback(context.Background())
		return nil, errors.New("TX 0")
	}

	if err := CompletePayment(tx, newId); err != nil {
		tx.Rollback(context.Background())
		log.Println(err)
		return nil, errors.New("TX 1")
	}

	if err := tx.Commit(context.Background()); err != nil {
		return nil, errors.New("TX 2")
	}

	return GetTransfer(id.String(), *sourceId)
}

func GetTransfer(uuid string, source int) (*models.Transfer, error) {
	conn := db.Default()

	row := conn.QueryRow(QueryGetTransfer, uuid, source)

	var transfer models.Transfer = models.Transfer{}

	var sourceId, destinationId int

	err := row.Scan(&transfer.ID, &transfer.Messgage, &transfer.Amount, &transfer.Status, &sourceId, &destinationId, &transfer.CreatedAt, &transfer.UpdatedAt)
	if err != nil {
		return nil, err
	}

	newSource, err := GetAccountByID(source)
	if err != nil {
		return nil, err
	}

	destination, err := GetAccountByID(destinationId)
	if err != nil {
		return nil, err
	}

	transfer.Source = newSource
	transfer.Destination = destination

	return &transfer, nil
}

func CompletePayment(tx pgx.Tx, uuid string) error {
	row, err := tx.Exec(context.Background(), QueryCompleteTransfer, uuid)

	if err != nil {
		return err
	}

	if row.RowsAffected() == 0 {
		return errors.New("No rows affected")
	}

	return nil
}
