package features

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/db"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
)

func CreateAccount(user *models.User, account *models.CreateAccountRequest) (*models.Account, error) {
	conn := db.Default()

	id, _ := uuid.NewUUID()
	newId := id.String()

	uid, err := GetUserID(user.ID)
	if err != nil {
		return nil, err
	}

	_, err = conn.Exec(QueryCreateAccount, newId, account.Name, *uid, time.Now())
	if err != nil {
		return nil, err
	}

	newAccount, err := GetAccount(id.String())
	if err != nil {
		return nil, err
	}

	return newAccount, nil
}

func getAccount(query string, subject ...interface{}) (*models.Account, error) {
	conn := db.Default()

	row := conn.QueryRow(query, subject...)

	var account models.Account

	err := row.Scan(&account.ID, &account.Name, &account.Balance, &account.CreatedAt, &account.UpdatedAt, &account.Currency)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

func GetAccount(id string) (*models.Account, error) {
	return getAccount(QueryGetAccount, id)
}

func GetAccountByID(id int) (*models.Account, error) {
	return getAccount(QueryGetAccountByID, id)
}

func GetUserAccounts(user *models.User) ([]*models.Account, error) {
	conn := db.Default()

	uid, err := GetUserID(user.ID)
	if err != nil {
		return nil, err
	}

	accountList := make([]*models.Account, 0)
	rows, err := conn.Query(QueryGetUserAccounts, uid)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var account models.Account
		err := rows.Scan(&account.ID, &account.Name, &account.Balance, &account.CreatedAt, &account.UpdatedAt, &account.Currency)
		if err != nil {
			return nil, err
		}

		accountList = append(accountList, &account)
	}

	rows.Close()
	return accountList, nil
}

func GetUserAccount(user *models.User, uuid string) (*models.Account, error) {
	conn := db.Default()

	uid, err := GetUserID(user.ID)
	if err != nil {
		return nil, err
	}

	row := conn.QueryRow(QueryGetUserAccount, uid, uuid)
	if err != nil {
		return nil, err
	}

	var account models.Account

	err = row.Scan(&account.ID, &account.Name, &account.Balance, &account.CreatedAt, &account.UpdatedAt, &account.Currency)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

const (
	QueryRechargeAccount = `UPDATE accounts SET balance = balance + $1, updated_at = $3 WHERE uuid = $2`
)

var (
	NoEffectError = errors.New("You can't withdraw more money than you have")
)

func RechargeAccount(account *models.Account, req *models.RechargeAccountRequest) (*models.Account, error) {
	conn := db.Default()

	_, err := conn.ExecTx(QueryRechargeAccount, req.Amount, account.ID, time.Now())
	if err != nil {
		return nil, err
	}

	newAccount, err := GetAccount(account.ID)
	if err != nil {
		return nil, err
	}

	return newAccount, nil
}

func WithdrawAccount(account *models.Account, req *models.RechargeAccountRequest) (*models.Account, error) {
	conn := db.Default()

	rows, err := conn.ExecTx(QueryWithdrawAccount, req.Amount, account.ID, time.Now())
	if err != nil {
		return nil, err
	}

	if rows == 0 {
		return nil, NoEffectError
	}

	newAccount, err := GetAccount(account.ID)
	if err != nil {
		return nil, err
	}

	return newAccount, nil
}

func GetAccountID(uuid string) (*int, error) {
	conn := db.Default()

	row := conn.QueryRow(QueryGetAccountID, uuid)

	var uid int

	err := row.Scan(&uid)
	if err != nil {
		return nil, err
	}

	return &uid, nil

}
