package middlewares

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/features"
	types "gitlab.com/mobwa/mobwa-api-server/pkg"
)

func BasicAuth(router *mux.Router) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			u, p, ok := r.BasicAuth()
			if !ok {
				w.Header().Set("Content-Type", types.ApplicationJSON)
				w.WriteHeader(http.StatusCreated)
				json.NewEncoder(w).Encode(types.UnauthorizedError([]string{"There were not provided credentials"}))
				return
			}

			user, err := features.FindUser(u, p)

			if err != nil {
				w.Header().Set("Content-Type", types.ApplicationJSON)
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(types.UnauthorizedError([]string{"The provided credentials are invalid"}))
				return
			}

			ctx := r.Context()
			ctx = context.WithValue(ctx, "user", *user)

			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)

		})

	}
}
