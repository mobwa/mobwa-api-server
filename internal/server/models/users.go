package models

import "time"

type UpdateUserResponse = User
type GetUserResponse = User
type ListUsersResponse = []User

type User struct {
	ID        string     `json:"id" json:"id"`
	Username  string     `json:"username" json:"username"`
	Email     string     `json:"email" json:"email"`
	Password  string     `json:"password,omitempty" json:"password,omitempty"`
	Role      string     `json:"role" json:"role"`
	CreatedAt time.Time  `json:"created_at" yaml:"created_at"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" yaml:"updated_at,omitempty"`
	Accounts  []*Account `json:"accounts,omitempty" yaml:"accounts,omitempty"`
}

type CreateUserRequest struct {
	Username string `json:"username" json:"username"`
	Email    string `json:"email" json:"email"`
	Password string `json:"password" json:"password"`
}
