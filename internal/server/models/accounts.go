package models

import "time"

type GetAccountResponse = Account
type CreateAccountResponse = Account
type UpdateAccountResponse = Account
type ListAccountResponse = []Account

type Account struct {
	ID        string     `json:"id" yaml:"id"`
	Name      string     `json:"name" yaml:"name"`
	Balance   int        `json:"balance" yaml:"balance"`
	CreatedAt time.Time  `json:"created_at" yaml:"created_at"`
	Currency  string     `json:"currency" yaml:"currency"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" yaml:"updated_at,omitempty"`
}

type CreateAccountRequest struct {
	Name     string `json:"name" yaml:"name"`
	Currency string `json:"currency" yaml:"currency"`
}

type UpdateAccountRequest struct {
	Name     string `json:"name" yaml:"name"`
	Currency string `json:"currency" yaml:"currency"`
}

type RechargeAccountRequest struct {
	Amount int `json:"amount" yaml:"amount"`
}
