package models

import "time"

type GetTransferResponse = Transfer
type CreateTransferResponse = Transfer
type UpdateTransferResponse = Transfer
type ListTransfersResponse = []Transfer
type Transfers = []Transfer

type Transfer struct {
	ID          string     `json:"id" yaml:"id"`
	Messgage    string     `json:"message" yaml:"message"`
	Amount      int        `json:"amount" yaml:"amount"`
	Currency    string     `json:"currency" yaml:"currency"`
	Status      string     `json:"status" yaml:"status"`
	Source      *Account   `json:"source,omitempty" yaml:"source,omitempty"`
	Destination *Account   `json:"destination,omitempty" yaml:"destination,omitempty"`
	CreatedAt   time.Time  `json:"created_at" yaml:"created_at"`
	UpdatedAt   *time.Time `json:"updated_at" yaml:"updated_at"`
}

type CreateTransferRequest struct {
	Message     string `json:"message" yaml:"message"`
	Amount      int    `json:"amount" yaml:"amount"`
	Currency    string `json:"currency" yaml:"currency"`
	Destination string `json:"destination" yaml:"destination"`
}

type UpdateTransferRequest struct {
	Message string `json:"message" yaml:"nmessage"`
}
