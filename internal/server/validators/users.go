package validators

import "gitlab.com/mobwa/mobwa-api-server/internal/server/models"

func CreateUser(data map[string]interface{}) (*models.CreateUserRequest, []string) {
	errs := make([]string, 0)

	newUser := &models.CreateUserRequest{}

	if email, ok := data["email"]; !ok {
		errs = append(errs, "email is required")
	} else {
		value, ok := email.(string)
		if !ok {
			errs = append(errs, "email must a string")
		} else if value == "" {
			errs = append(errs, "email must not be a empty string")
		}

		newUser.Email = value
	}

	if username, ok := data["username"]; !ok {
		errs = append(errs, "username is required")
	} else {
		value, ok := username.(string)
		if !ok {
			errs = append(errs, "username must a string")
		} else if value == "" {
			errs = append(errs, "username must not be a empty string")
		}

		newUser.Username = value
	}

	if password, ok := data["password"]; !ok {
		errs = append(errs, "password is required")
	} else {
		value, ok := password.(string)
		if !ok {
			errs = append(errs, "password must a string")
		}

		if value == "" {
			errs = append(errs, "password must not be a empty string")
		}

		newUser.Password = value
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return newUser, nil
}
