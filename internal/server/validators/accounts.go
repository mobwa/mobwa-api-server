package validators

import (
	"strings"

	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
)

func CreateAccount(data map[string]interface{}) (*models.CreateAccountRequest, []string) {
	errs := make([]string, 0)

	newAccount := &models.CreateAccountRequest{}

	if name, ok := data["name"]; !ok {
		errs = append(errs, "name is required")
	} else {
		value, ok := name.(string)
		if !ok {
			errs = append(errs, "name must a string")
		} else if value == "" {
			errs = append(errs, "name must not be a empty string")
		}

		newAccount.Name = value
	}

	if currency, ok := data["currency"]; !ok {
		errs = append(errs, "currency is required")
	} else {
		value, ok := currency.(string)
		if !ok {
			errs = append(errs, "currency must a string")
		} else if value == "" {
			errs = append(errs, "currency must not be a empty string")
		} else if value != strings.ToUpper("USD") || value != strings.ToUpper("SGD") {
			errs = append(errs, "currency must be one of USD or SGD")
		}

		newAccount.Name = strings.ToUpper(value)
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return newAccount, nil
}

func RechargeAccount(data map[string]interface{}) (*models.RechargeAccountRequest, []string) {
	errs := make([]string, 0)

	newRecharge := &models.RechargeAccountRequest{}

	if amount, ok := data["amount"]; !ok {
		errs = append(errs, "amount is required")
	} else {
		value, ok := amount.(float64)
		if !ok {
			errs = append(errs, "amount must a number")
		} else if value := int(value); value > 0 {
			newRecharge.Amount = value
		} else {
			errs = append(errs, "amount must be greater than 0")
		}
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return newRecharge, nil
}

func UpdateAccount(data map[string]interface{}) (*models.CreateAccountRequest, []string) {
	errs := make([]string, 0)

	newAccount := &models.CreateAccountRequest{}

	if currency, ok := data["currency"]; !ok {
		errs = append(errs, "currency is required")
	} else {
		value, ok := currency.(string)
		if !ok {
			errs = append(errs, "currency must a string")
		} else if value == "" {
			errs = append(errs, "currency must not be a empty string")
		} else if value != strings.ToUpper("USD") || value != strings.ToUpper("SGD") {
			errs = append(errs, "currency must be one of USD or SGD")
		}

		newAccount.Currency = strings.ToUpper(value)
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return newAccount, nil
}
