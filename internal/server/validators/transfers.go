package validators

import (
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
)

func CreateTransfer(data map[string]interface{}) (*models.CreateTransferRequest, []string) {
	errs := make([]string, 0)

	transfer := models.CreateTransferRequest{}

	if message, ok := data["message"]; !ok {
		errs = append(errs, "message is required")
	} else {
		value, ok := message.(string)
		if !ok {
			errs = append(errs, "message must a string")
		} else if value == "" {
			errs = append(errs, "message must not be a empty string")
		}

		transfer.Message = value
	}

	if destination, ok := data["destination"]; !ok {
		errs = append(errs, "destination is required")
	} else {
		value, ok := destination.(string)
		if !ok {
			errs = append(errs, "destination must a string")
		}

		if value == "" {
			errs = append(errs, "destination must not be a empty string")
		}

		transfer.Destination = value
	}

	if amount, ok := data["amount"]; !ok {
		errs = append(errs, "amount is required")
	} else {
		value, ok := amount.(float64)
		if !ok {
			errs = append(errs, "amount must an integer")
		}

		if value < 0 {
			errs = append(errs, "amount must greater than 0")
		}

		transfer.Amount = int(value)
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return &transfer, nil
}

func UpdateTransfer(data map[string]interface{}) (*models.CreateTransferRequest, []string) {
	errs := make([]string, 0)

	transfer := &models.CreateTransferRequest{}

	if message, ok := data["message"]; ok {
		value, ok := message.(string)
		if !ok {
			errs = append(errs, "message must a string")
		} else if value == "" {
			errs = append(errs, "message must not be a empty string")
		}

		transfer.Message = value
	}

	if _, ok := data["receiver"]; ok {
		errs = append(errs, "receiver should not change")
	}

	if _, ok := data["currency"]; ok {
		errs = append(errs, "currency should not change")
	}

	if amount, ok := data["amount"]; ok {
		value, ok := amount.(float64)
		if !ok {
			errs = append(errs, "amount must an integer")
		}

		if value < 0 {
			errs = append(errs, "amount must greater than 0")
		}

		transfer.Amount = int(value)
	}

	if len(errs) > 0 {
		return nil, errs
	}

	return transfer, nil
}
