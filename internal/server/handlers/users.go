package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mobwa/mobwa-api-server/internal/server/features"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/validators"
	types "gitlab.com/mobwa/mobwa-api-server/pkg"
)

const (
	DefaultAccountName     = "main"
	DefaultAccountCurrency = "SGD"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		user, errs := validators.CreateUser(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		newUser, err := features.CreateUser(*user)
		if err != nil {
			if err == features.ErrUserExists {
				err := types.ConflictError([]string{err.Error()})
				w.WriteHeader(err.Code)
				json.NewEncoder(w).Encode(err)
				return
			}
		}

		newAccountReq := &models.CreateAccountRequest{Name: DefaultAccountName, Currency: DefaultAccountCurrency}
		account, err := features.CreateAccount(newUser, newAccountReq)
		if err != nil {
			err := types.InternalServerError([]string{err.Error()})
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		newUser.Accounts = []*models.Account{account}

		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(&newUser)
	} else {
		err := types.BadRequestError(types.InvalidJSONError)
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}
