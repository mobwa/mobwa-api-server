package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/features"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/validators"
	types "gitlab.com/mobwa/mobwa-api-server/pkg"
)

func CreateTransfer(w http.ResponseWriter, r *http.Request) {
	accountId := mux.Vars(r)["accountId"]

	var data map[string]interface{}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		transferData, errs := validators.CreateTransfer(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		user, err := ExtractUserFromContext(w, r)
		if err != nil {
			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
			var newErr *types.HTTPError
			if err == UserNotFoundError {
				newErr = types.NotFoundError([]string{err.Error()})
			} else {
				newErr = types.InternalServerError([]string{err.Error()})
			}

			w.WriteHeader(newErr.Code)
			json.NewEncoder(w).Encode(newErr)

			return
		}

		account, err := features.GetAccount(accountId)
		if err != nil {
			err := types.NotFoundError([]string{err.Error()})
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		transfer, err := features.CreateTransfer(user, account, transferData)
		if err != nil {
			if err == features.ErrDestinationAccountAbsent {
				err := types.NotFoundError([]string{err.Error()})
				w.WriteHeader(err.Code)
				json.NewEncoder(w).Encode(err)
			} else if err == features.ErrCurrencyMismatch {
				err := types.BadRequestError([]string{err.Error()})
				w.WriteHeader(err.Code)
				json.NewEncoder(w).Encode(err)
			} else if err == features.ErrInsuficientBalance {
				err := types.BadRequestError([]string{err.Error()})
				w.WriteHeader(err.Code)
				json.NewEncoder(w).Encode(err)
			} else {
				err := types.InternalServerError([]string{err.Error()})
				w.WriteHeader(err.Code)
				json.NewEncoder(w).Encode(err)
			}

			return
		}

		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(transfer)

	} else {
		err := types.BadRequestError(types.InvalidJSONError)
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}

func UpdateTransfer(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		transfer, errs := validators.UpdateTransfer(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
		} else {
			w.WriteHeader(http.StatusCreated)
			json.NewEncoder(w).Encode(&transfer)
		}
	} else {
		err := types.BadRequestError(types.InvalidJSONError)
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}
