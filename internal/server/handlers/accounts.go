package handlers

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/features"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/models"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/validators"
	types "gitlab.com/mobwa/mobwa-api-server/pkg"
)

var (
	UserNotFoundError    = errors.New("Did not find user")
	UnknownUserError     = errors.New("Did not find user")
	AccountNotFoundError = errors.New("The account with specified ID does not exist")
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		newAccountReq, errs := validators.CreateAccount(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		user, err := ExtractUserFromContext(w, r)
		if err != nil {
			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
			var newErr *types.HTTPError
			if err == UserNotFoundError {
				newErr = types.NotFoundError([]string{err.Error()})
			} else {
				newErr = types.InternalServerError([]string{err.Error()})
			}

			w.WriteHeader(newErr.Code)
			json.NewEncoder(w).Encode(newErr)

			return
		}

		account, err := features.CreateAccount(user, newAccountReq)
		if err != nil {
			err := types.InternalServerError([]string{err.Error()})
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		user.Accounts = []*models.Account{account}

		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(&user)
	} else {
		err := types.BadRequestError(types.InvalidJSONError)
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}

func GetAccount(w http.ResponseWriter, r *http.Request) {
	accountId := mux.Vars(r)["accountId"]

	user, err := ExtractUserFromContext(w, r)
	if err != nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		var newErr *types.HTTPError
		if err == UserNotFoundError {
			newErr = types.NotFoundError([]string{err.Error()})
		} else {
			newErr = types.InternalServerError([]string{err.Error()})
		}

		w.WriteHeader(newErr.Code)
		json.NewEncoder(w).Encode(newErr)

		return
	}

	accounts, err := features.GetUserAccount(user, accountId)
	if err != nil {
		if err == pgx.ErrNoRows {
			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
			err := types.NotFoundError(types.AbsentAccountError)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		err := types.InternalServerError([]string{err.Error()})
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)

		return
	}

	w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&accounts)
}

func ListAccounts(w http.ResponseWriter, r *http.Request) {
	user, err := ExtractUserFromContext(w, r)
	if err != nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		var newErr *types.HTTPError
		if err == UserNotFoundError {
			newErr = types.NotFoundError([]string{err.Error()})
		} else {
			newErr = types.InternalServerError([]string{err.Error()})
		}

		w.WriteHeader(newErr.Code)
		json.NewEncoder(w).Encode(newErr)

		return
	}

	accounts, err := features.GetUserAccounts(user)
	if err != nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		err := types.InternalServerError([]string{err.Error()})
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)

		return
	}
	w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&accounts)
}

func RechargeAccount(w http.ResponseWriter, r *http.Request) {
	accountId := mux.Vars(r)["accountId"]

	var data map[string]interface{}

	user, err := ExtractUserFromContext(w, r)
	if err != nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		var newErr *types.HTTPError
		if err == UserNotFoundError {
			newErr = types.NotFoundError([]string{err.Error()})
		} else {
			newErr = types.InternalServerError([]string{err.Error()})
		}

		w.WriteHeader(newErr.Code)
		json.NewEncoder(w).Encode(newErr)

		return
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		_, errs := validators.RechargeAccount(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		account, err := features.GetUserAccount(user, accountId)
		if err != nil {
			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

			var newErr *types.HTTPError
			if err == pgx.ErrNoRows {
				newErr = types.NotFoundError([]string{AccountNotFoundError.Error()})
			} else {
				newErr = types.InternalServerError([]string{err.Error()})
			}

			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
			w.WriteHeader(newErr.Code)
			json.NewEncoder(w).Encode(newErr)

		}

		rechargeReq, errs := validators.RechargeAccount(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)
			return
		}

		account, err = features.RechargeAccount(account, rechargeReq)
		if err != nil {

		}

		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(&account)
	} else {
		err := types.BadRequestError([]string{"input data is not a valid JSON document"})
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}

func ExtractUserFromContext(w http.ResponseWriter, r *http.Request) (*models.User, error) {
	var user models.User

	if userCtx := r.Context().Value("user"); userCtx != nil {
		if newUser, ok := userCtx.(models.User); ok {
			user = newUser
		} else {
			return nil, UnknownUserError
		}
	} else {
		return nil, UserNotFoundError
	}

	return &user, nil
}

func WithdrawAccount(w http.ResponseWriter, r *http.Request) {
	accountId := mux.Vars(r)["accountId"]

	var data map[string]interface{}

	user, err := ExtractUserFromContext(w, r)
	if err != nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		var newErr *types.HTTPError
		if err == UserNotFoundError {
			newErr = types.NotFoundError([]string{err.Error()})
		} else {
			newErr = types.InternalServerError([]string{err.Error()})
		}

		w.WriteHeader(newErr.Code)
		json.NewEncoder(w).Encode(newErr)

		return
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err == nil {
		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

		_, errs := validators.RechargeAccount(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		account, err := features.GetUserAccount(user, accountId)
		if err != nil {
			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)

			var newErr *types.HTTPError
			if err == pgx.ErrNoRows {
				newErr = types.NotFoundError([]string{AccountNotFoundError.Error()})
			} else {
				newErr = types.InternalServerError([]string{err.Error()})
			}

			w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
			w.WriteHeader(newErr.Code)
			json.NewEncoder(w).Encode(newErr)

			return
		}

		rechargeReq, errs := validators.RechargeAccount(data)
		if len(errs) > 0 {
			err := types.BadRequestError(errs)
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		account, err = features.WithdrawAccount(account, rechargeReq)
		if err != nil {
			err := types.BadRequestError([]string{err.Error()})
			w.WriteHeader(err.Code)
			json.NewEncoder(w).Encode(err)

			return
		}

		w.Header().Set(types.HeaderContentType, types.ApplicationJSON)
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(&account)
	} else {
		err := types.BadRequestError(types.InvalidJSONError)
		w.WriteHeader(err.Code)
		json.NewEncoder(w).Encode(err)
	}
}
