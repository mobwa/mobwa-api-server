package db

import (
	"log"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/lib/pq"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/db/migrations"
)

func Migrate(dsn string) error {
	s := bindata.Resource(migrations.AssetNames(),
		func(name string) ([]byte, error) {
			log.Println(name)
			return migrations.Asset(name)
		})

	data, err := bindata.WithInstance(s)
	if err != nil {
		return err
	}

	m, err := migrate.NewWithSourceInstance("go-bindata", data, dsn)
	if err != nil {
		return err
	}

	log.Println("Runnig migrations")

	if err := m.Up(); err != nil {
		if err == migrate.ErrNoChange {
			log.Println("There are no changes in database")
		} else if err == migrate.ErrLockTimeout {
			log.Println("There are no changes in database")
		} else if err == migrate.ErrLocked {
			log.Println("There are no changes in database")
		} else {
			return err
		}
	}

	return nil
}
