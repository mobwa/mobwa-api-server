package db

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

var connection *Connection
var newDSN dsn

func Default() *Connection {
	return connection
}

func DSN() string {
	return newDSN.String()
}

func Init(user, password, host, port, db string) error {
	newDSN = NewDSN(user, password, host, port, db)
	conn, err := New(newDSN)
	if err != nil {
		return err
	}

	connection = conn
	return nil
}

type dsn struct {
	user string
	pwd  string
	host string
	port string
	db   string
}

func (d dsn) String() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", d.user, d.pwd, d.host, d.port, d.db)
}

func NewDSN(user, pwd, host, port, db string) dsn {
	return dsn{user, pwd, host, port, db}
}

func New(d dsn) (*Connection, error) {
	db, err := pgx.Connect(context.Background(), d.String())
	if err != nil {
		return nil, err
	}

	newConn := Connection{
		ctx: context.Background(),
		db:  db,
	}

	return &newConn, nil
}

type Connection struct {
	ctx context.Context
	db  *pgx.Conn
}

func (c Connection) ExecTx(sql string, args ...interface{}) (int64, error) {
	tx, err := c.db.Begin(context.Background())
	if err != nil {
		return 0, err
	}

	r, err := tx.Exec(context.Background(), sql, args...)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return 0, err
	}

	return r.RowsAffected(), nil
}

func (c Connection) Exec(sql string, args ...interface{}) (pgconn.CommandTag, error) {
	return c.db.Exec(c.ctx, sql, args...)
}

func (c Connection) Tx() (pgx.Tx, error) {
	return c.db.Begin(context.Background())
}

func (c Connection) Query(sql string, args ...interface{}) (pgx.Rows, error) {
	return c.db.Query(c.ctx, sql, args...)
}

func (c Connection) QueryRow(sql string, args ...interface{}) pgx.Row {
	return c.db.QueryRow(c.ctx, sql, args...)
}

func (c Connection) Terminate() error {
	return c.db.Close(c.ctx)
}
