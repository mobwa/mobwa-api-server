CREATE TABLE accounts (
	id SERIAL PRIMARY KEY,
    uuid VARCHAR (50) UNIQUE NOT NULL,
    name VARCHAR (50) NOT NULL,
    user_id INT NOT NULL,
    balance INT NOT NULL DEFAULT 0,
	created_at TIMESTAMP NOT NULL,
    currency VARCHAR(50) NOT NULL DEFAULT 'USD',
    updated_at TIMESTAMP ,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    UNIQUE(user_id, name)
);