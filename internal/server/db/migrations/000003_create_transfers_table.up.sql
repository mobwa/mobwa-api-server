CREATE TABLE transfers (
	id serial PRIMARY KEY,
    uuid VARCHAR (128) UNIQUE NOT NULL,
    message VARCHAR (128) NOT NULL,
    source INT NOT NULL,
    destination INT NOT NULL,
    amount INT NOT NULL,
    currency VARCHAR NOT NULL,
    status VARCHAR NOT NULL DEFAULT 'PENDING',
	created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    FOREIGN KEY (source) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (destination) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE CASCADE
);