package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/handlers"
	"gitlab.com/mobwa/mobwa-api-server/internal/server/middlewares"
)

func Router() *mux.Router {
	newRouter := mux.NewRouter().StrictSlash(true)

	signupRouter := newRouter.PathPrefix("/signup").Subrouter()
	registerHandler(signupRouter, http.MethodPost, "", handlers.CreateUser, "signup")

	usersRouter := newRouter.PathPrefix("/users").Subrouter()
	registerHandler(usersRouter, http.MethodPost, "", handlers.CreateUser, "create-user")
	// registerHandler(usersRouter, http.MethodGet, "/{userId}", handlers.GetUser, "get-user")
	usersRouter.Use(middlewares.BasicAuth(usersRouter))

	transfersRouter := newRouter.PathPrefix("/accounts/{accountId}/transfers").Subrouter()
	// registerHandler(transfersRouter, http.MethodGet, "", handlers.ListTransfers, "list-transfers")
	// registerHandler(transfersRouter, http.MethodGet, "/{transferId}", handlers.GetTransfer, "get-transfer")
	registerHandler(transfersRouter, http.MethodPost, "", handlers.CreateTransfer, "create-transfer")
	// registerHandler(transfersRouter, http.MethodDelete, "/transfers/{transferId}", handlers.GetTransfer, "delete-transfer")
	transfersRouter.Use(middlewares.BasicAuth(transfersRouter))

	accountsRouter := newRouter.PathPrefix("/accounts").Subrouter()
	registerHandler(accountsRouter, http.MethodGet, "", handlers.ListAccounts, "list-accounts")
	registerHandler(accountsRouter, http.MethodGet, "/{accountId}", handlers.GetAccount, "get-account")
	registerHandler(accountsRouter, http.MethodPost, "/{accountId}/recharge", handlers.RechargeAccount, "recharge-account")
	registerHandler(accountsRouter, http.MethodPost, "/{accountId}/withdraw", handlers.WithdrawAccount, "withdraw-account")

	// registerHandler(accountsRouter, http.MethodPut, "{accountId}/transfers/{transferId}", transfers.UpdateTransfer, "update-transfer")
	accountsRouter.Use(middlewares.BasicAuth(accountsRouter))

	newRouter.HandleFunc("/metrics", promhttp.Handler().ServeHTTP)

	return newRouter
}

func registerHandler(router *mux.Router, method string, path string, handler http.HandlerFunc, name string) {
	router.HandleFunc(path, handler).Methods(method).Name(name)

}
