package config

var Config = &config{
	API: api{},
	DB:  db{},
}

type config struct {
	API api `json:"api"`
	DB  db  `json:"db"`
}

type api struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type db struct {
	Name     string `json:"name"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	Password string `json:"password"`
	User     string `json:"user"`
}
