package cli

type Error struct {
	Code int
	Err  error
}

func (c *Error) Error() string {
	return c.Err.Error()
}
