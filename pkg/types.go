package types

import "net/http"

const (
	HeaderContentType = "Content-Type"
	ApplicationJSON   = "application/json"
	ErrWithUser       = "Something is wrong with the user"
)

var (
	InvalidJSONError   = []string{"input data is not a valid JSON document"}
	AbsentAccountError = []string{"The account with specified ID does not exist"}
)

type HTTPError struct {
	Code    int         `json:"code" yaml:"code"`
	Errors  interface{} `json:"errors,omitempty" yaml:"errors,omitempty"`
	Message string      `json:"msg,omitempty" yaml:"msg,omitempty"`
}

func NewHTTPError(code int, msg []string) *HTTPError {
	return &HTTPError{Code: code, Errors: msg}
}

func InternalServerError(msg []string) *HTTPError {
	return &HTTPError{Code: http.StatusInternalServerError, Errors: msg}
}

func BadRequestError(msg []string) *HTTPError {
	return &HTTPError{Code: http.StatusBadRequest, Errors: msg}
}

func UnauthorizedError(msg []string) *HTTPError {
	return &HTTPError{Code: http.StatusUnauthorized, Errors: msg}
}

func NotFoundError(msg []string) *HTTPError {
	return &HTTPError{Code: http.StatusNotFound, Errors: msg}
}

func ConflictError(msg []string) *HTTPError {
	return &HTTPError{Code: http.StatusConflict, Errors: msg}
}
