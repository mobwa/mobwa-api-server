package main

import (
	"gitlab.com/mobwa/mobwa-api-server/internal/app"
)

func main() {
	app.New().Run()
}
